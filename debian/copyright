Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sbbi-upnplib
Upstream-contact: Chris Duncan <http://triplea.sourceforge.net/mywiki/TripleA>
Source: http://sourceforge.net/p/triplea/code/HEAD/tree/upnp/

Files:*
Copyright: 2002 SuperBonBon Industries, 2013 Chris Duncan
License: Apache-1.1
 Redistribution and use in source and binary forms, with
 or without modification, are permitted provided that the
 following conditions are met: 1. Redistributions of source code
 must retain the above copyright notice, this list of conditions and
 the following disclaimer. 2. Redistributions in binary form must
 reproduce the above copyright notice, this list of conditions and
 the following disclaimer in the documentation and/or other
 materials provided with the distribution. 3. The end-user
 documentation included with the redistribution, if any, must
 include the following acknowledgment: "This product includes
 software developed by SuperBonBon Industries
 (http://www.sbbi.net/)." Alternately, this acknowledgment may
 appear in the software itself, if and wherever such third-party
 acknowledgments normally appear. 4. The names "UPNPLib" and
 "SuperBonBon Industries" must not be used to endorse or promote
 products derived from this software without prior written
 permission. For written permission, please contact info@sbbi.net.
 5. Products derived from this software may not be called
 "SuperBonBon Industries", nor may "SBBI" appear in their name,
 without prior written permission of SuperBonBon Industries. THIS
 SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR ITS
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLU- DING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE. This software consists of voluntary contributions made
 by many individuals on behalf of SuperBonBon Industries. For more
 information on SuperBonBon Industries, please see
 <http://www.sbbi.net/>.

Files: debian/*
Copyright: Copyright 2014 Scott Howard <showard@debian.org>
License: Expat
 Permission  is  hereby granted, free of charge, to any person ob‐
 taining a copy of  this  software  and  associated  documentation
 files  (the  Software),  to deal in the Software without restric‐
 tion, including without limitation the rights to use, copy, modi‐
 fy, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is  fur‐
 nished to do so, subject to the following conditions:
 .
 The  above  copyright  notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EX‐
 PRESS  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR  A  PARTICULAR  PURPOSE  AND  NONIN‐
 FRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  IN  AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  THE
 SOFTWARE.
